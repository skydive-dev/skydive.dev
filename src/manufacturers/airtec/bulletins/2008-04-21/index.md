---
pageTitle: CYPRES 2 Service Bulletin C2 0408
date: 2008-04-21
layout: bulletin.html
collection:
  - bulletins
  - airtec
short_description: Recall of certain CYPRES 2 units
description: Recall of certain CYPRES 2 units
status: Mandatory.
identification: CYPRES 2 from the production period during August 2006 to December 2006. The affected serial numbers are listed on the pages number 2 & 3. 
source: https://www.sskinc.com/images/Res/files/SB/CYPRES_Service_Bulletin_April_2008%20e1.pdf
source_accessed: March 7, 2019
---

**ℹ️ NOTE:** Airtec issued a follow-up letter [available here](followup.pdf)

### Background

Earlier this year we experienced two unwanted activations in Australia. Nobody was hurt. Both units have been examined extensively at Airtec, with the finding that a defective sensor caused the activation in both cases. As both units passed the more than 14 days period manufacturing testing process without any noticable problems, we must assume that it is possible that there are more units of this production batch in use with a similar sensor defect. As a precaution, we have decided to recall all potentially affected CYPRES 2‘s for a preventive sensor replacement. 

### Service Bulletin

Please compare the serial number of each CYPRES 2 with the [attached list](source.pdf#page=2). Note that it is not necessary to open the reserve container, as it is easy to check the CYPRES 2 serial number on the display. (See your CYPRES 2 User Guide for more info.)
In case your unit is affected, please forward it to Airtec - Germany, or to SSK Industries in the US. All necessary work will be done as quick as possible and free of charge, including return shipment. As compensation Airtec will include a check in the amount of $ 70.00 US for each affected unit upon its return. Treated units will wear a sticker and have a certification.

We appologize for the inconvenience this may cause, however we feel it is necessary to ensure
your safety.

U.S. shipping address: 

SSK Industries Inc<br>
1008 Monroe Road<br>
Lebanon, OH 45036<br>
PH 513-934-3201<br>
service@CYPRES-USA.com

### Compliance Date

Effective immediately, before the next jump.

### Authority

Airtec GmbH & Co. KG Safety Systems<br>
Helmut Cloth<br>
Mittelstr. 69, 33181 Bad Wünnenberg, Germany<br>
Tel: +49 2953 98990<br>
Fax: +49 2953 1293<br>
service@cypres.cc

### Distribution

All dealers<br>
Parachuting publications<br>
Parachute Industry Association
