---
pageTitle: CYPRES 2 Service Bulletin C2 0113
date: 2013-01-31
layout: bulletin.html
collection:
  - bulletins
  - airtec
short_description: Please check your CYPRES 2 date of manufacture before next use!
description: Important! Please check your CYPRES 2 date of manufacture before next use!
status: MANDATORY before each jump.
identification: CYPRES 2 units manufactured February 2009 through December 2012 (02/2009 - 12/2012).
source: https://www.sskinc.com/images/Res/files/SB/SB_31012013_eng.pdf
source_accessed: March 7, 2019
---

**ℹ️ NOTE:** Airtec issued an addendum (below), and also a [FAQ](faq.pdf).

### Background

Airtec GmbH & Co. KG has become aware of a small number of CYPRES 2 units becoming “non-responsive." Although the subject units indicated a “0” (or the selected DZ setting) on the display, they were no longer operating. Extensive research indicates that this situation is extremely rare and tends to occur during packing, especially on non-static-proof surfaces such as plastic, nylon or carpeted areas. Low humidity, build-up of static electricity and changing environmental conditions are all contributing factors. In addition, a recent activation, after the rig had been placed on the packing mat, has been linked to this phenomenon. 

### Action to be taken

**Prior to each jump**, during your pre-boarding equipment check (**after** the CYPRES has been switched on), perform
the following system test:

Click the control unit push button one time, and watch for the red LED light to flash. A flash indicates that the unit is working properly. If the red LED does not flash, repeat this procedure to confirm. **IF THE UNIT IS NOT RESPONDING, IT IS NOT IN A SAFE WORKING CONDITION, AND THE UNIT WILL NOT FUNCTION AS INTENDED ON A JUMP.** If this is the case, contact Airtec GmbH & Co. KG or SSK Industries, Inc. for further instructions either to arrange for repair, or for a loaner or replacement CYPRES 2.

**If the unit is not in working condition, failure to perform this procedure prior to each jump will result in an increased risk to the user.**

### Resolution

CYPRES 2 units manufactured after 1 January 2013 (01/01/2013) contain an update to prevent this situation from
occurring.

All existing CYPRES 2 units in the field manufactured during the affected date range (identified above) will receive
the update as they cycle through their periodic maintenance requirement, or during other repairs. After a CYPRES 2
unit receives the update, the procedure described above (clicking the control unit push button during each preboarding equipment check) is no longer required. 

### Additional Technical Details

The reason that only units manufactured during the above date range are affected is because the manufacturer of a component made a change on an internal ASIC microcircuit (which is something like a processor) to a higher level of integration. Prior to accepting the revised component, Airtec went through a 13-month evaluation period. This included laboratory testing as well as field-testing of 151 CYPRES units with no events or anomalies experienced. The revised component entered CYPRES production in February 2009. 

### Authority

Helmut Cloth<br>
Airtec GmbH & Co. KG Safety Systems<br>
Mittelstrasse 69<br>
33181 Bad Wünnenberg, Germany<br>
Tel: +49 2953-9899-0<br>
Fax: +49 2953-1293<br>
info@cypres.cc 

### Distribution

North America Market & Dealers<br>
North America Parachuting Publications<br>
Parachute Industry Association

### Addendum - May 2014

<p><strong>📄 Source:</strong> <a href="addendum.pdf">PDF</a> <span class="light">(<a href="https://www.sskinc.com/images/Res/files/SB/SB-Follow-up_05_2014_eng.pdf">Original</a> accessed: March 7, 2019)</span></p>

IMPORTANT Follow-Up to CYPRES 2 Service Bulletin C2 0113 – Additional included serial numbers and other important information. PLEASE READ

In a follow-up to Service Bulletin C2 0113, Airtec GmbH & Co. KG and SSK Industries, Inc. are accepting CYPRES units affected by the Service Bulletin for premature updates. Since we do not know how many units will be sent in, we cannot give a definite turnaround time, but we will do it as quickly as we can.

The maintenance update consists of the installation of additional hardware and new software and is free of charge, but since the Service Bulletin is not a recall, we will not bear the cost for the shipping in either direction, for reserve repacks or for other costs involved. As a reminder, the “control unit push button/LED flash check,” prior to each jump (when the CYPRES is switched on), as described in Service Bulletin C2 0113, is the alternative to a premature update. During the next scheduled maintenance, the Service Bulletin update will be performed automatically without additional costs or additional service time. This may be more convenient, as an interim solution, than sending the unit back for service before its scheduled maintenance. The choice is yours to make.

As a result of our ongoing Quality Management Program, we have added additional CYPRES [serial numbers](http://www.cypres.cc/images/stories/bulletin/SB_31012013_FU_DEVICES.PDF) to those included in Service Bulletin SB C2 0113. Fortunately, nearly all of these added units will be due for their regular maintenance within the next 10 months. The additional hardware and new software will be automatically installed at that time, without additional time or costs to you. The “control unit push button/LED flash check” (when the CYPRES is switched on), before each jump (which takes only about 1.5 seconds to perform) will, therefore, only be needed for one year or less. We ask that you compare the serial number of your CYPRES with serial numbers on the list below. You can find your serial number easily via the display. Switch on the unit, as soon as the self-test has reached “0” press and hold the button until the serial number will be displayed. If your serial number is listed in this notice, please visit the FAQ for further details and for the necessary steps on how to proceed.

We apologize for any inconvenience, but we are taking these measures because your safety is
our greatest concern.
