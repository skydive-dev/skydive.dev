---
pageTitle: CYPRES Service Bulletin
date: 1994-01-01
layout: bulletin.html
collection:
  - bulletins
  - airtec
short_description: Very important cypres dealer info
description: Very important cypres dealer info
status: Unknown.
identification: Unknown.
source: https://www.cypres.aero/wp-content/uploads/2016/08/recall1994.pdf
source_accessed: March 6, 2019
---

### Overview

Up until August 1993 Airtec produced 7600 Cypres without problem. A situation has now arisen where, because of a small modification in one integrated circuit which was not identified in our test procedures, we have produced units which under extremely rare circumstances might exceed the precise tolerances we demand.

As our policy is not to accept anything but the best we are recalling 1527 units released between 26 August 1993 and 2 November 1993.

We are therefore asking every owner affected by this to return his Cypres for checking and modiﬁcation as soon as possible.

Preparations have been made to ensure that units will be returned as soon as possible and at the most within 2 weeks of receipt at Airtec. We will refund $10 for shipping, send the unit back free of charge, and also, if the Cypres had already been installed, pay $40 for a reserve re—pack. Owners who feel that they cannot be without a Cypres should send a note and we will try to send a loan unit to use while theirs is being checked.

If you own a Cypres which is affected by this notice and you are in the Continental United States, please send your Cypres or your complete rig to:

#### UPS shipping address:

SSK Industries<br>
4925 N. State Rt. 42<br>
Waynesville, OH 45068

#### Postal shipping address:

SSK Industries<br>
PO. Box 635<br>
Waynesville, OH 45068

#### From elsewhere please ship to:

Airtec GmbH<br>
Mittelstr. 69<br>
33181 Wünnenberg<br>
Germany

If you are in any doubt as to whether or not your Cypres is affected please contact your dealer. He has a complete list of serial numbers from us. The following information might also be of help. All units delivered since the end of

November 1993 have numbers engraved on the side of the control unit (the bit you press to switch it on), at the opposite end to the cable. These Cypres are not affected.

If you are shipping abroad, please declare a notional low value of $10, stating that the unit is being returned for repair or replacement and not for resale. This will prevent any problems with customs and will help any undue delay.

We apologise for any inconvenience and thank you for helping us to maintain the highest standards of quality.

Please find attached a list of those Cypres units affected by this notice, Which you have received from us. As an additional aid to identification you should know that all Cypres released by us since the end of November 1993 have numbers engraved on the side of the control unit, at the oggosite end to the cable. These units are not affected

We would ask you to contact the owners of affected units and also, as far as you can, to answer any questions those owners might have.

Although the modiﬁcation has to be done here in Germany it would make sense for dealers in the Continental United States to go through 88K. This should be the easiest option unless you intend to send a large quantity and are happy to ship them direct to us.

If you are shipping abroad, please declare a notional low value of $10, stating that the units are being returned for repair or replacement and not for resale. This will prevent any problems with customs and will help any undue delay.

Should you incur any additional expenses when contacting or informing your customers etc., we would be pleased to receive your account.

Thank you very much for helping us to maintain the highest standards of quality.

### Battery life

Unfortunately the periodicity for Cypres battery changes remains - as stated in
the User's Guide — after 2 years or 500 jumps (whichever comes ﬁrst).

The batteries must also be changed at any time that the unit fails the self test due to a low battery error code (8996, 8998 or 8999); or if the Cypres switches itself off directly after the switching on sequence.

We are sorry that the period cannot be extended but have opted for absolute safety. We would ask you therefore, to do your utmost to change time expired batteries when you encounter them.
