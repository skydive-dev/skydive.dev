---
pageTitle: CYPRES 2 Service Bulletin C1/2 0108
date: 2008-01-22
layout: bulletin.html
collection:
  - bulletins
  - airtec
short_description: General handling if selftest is unsuccessful
description: Neccessary actions in case of an unsuccessful or incomplete selftest, Cypres1 and Cypres2.
status: Mandatory.
identification: All Cypres1 and Cypres2 units, all versions, in case of unsuccessful or incomplete selftest and/or switch-on procedure.
source: https://www.sskinc.com/images/Res/files/SB/Service_Bulletin_Jan_2008.pdf
source_accessed: March 6, 2019
---

### Background

On the 29th of December 2007 in Eloy, AZ, USA a Cypres2 unit activated a few seconds after exit.

The Cypres showed problems with the switch-on several times the days before. An attempt to switch on the unit before the skydive was made, and nothing was seen on the display, but it was decided to use the parachute anyway. Fortunately there were no injuries.

The technical inspection in Germany showed that this incident was a result of several factors.

A very abnormal power supply defect (transient battery voltage before failure) in combination with a specific flight situation made this activation possible.

After days of reconstructing and testing, we judge this as a very unique scenario. We tested a very high number of batteries in our house without any findings. There are no reports of similar cases.

### Service Bulletin

Please always observe the complete selftest until the „0“ appears.

In the event an error code is displayed, please consult the appropriate CYPRES User's Guide for appropriate action(s).

In the event that there are any irregularities or conditions during the selftest and/or switch-on procedure, which are not explained in the CYPRES User's Guide (such as unknown error codes or numbers, missing numbers, no red light, blank display, etc.) – please contact Airtec or SSK before the next jump.

### Compliance Date

Effective Immediately.

### Authority

Airtec GmbH & Co. KG<br>
Kai Koerner<br>
Tel: +49 2953 989948<br>
Fax:+49 2953 1293<br>
kai@cypres.cc<br>
Mittelstrasse 69<br>
33181 Bad Wuennenberg<br>
Germany

### Distribution

All dealers<br>
Parachuting publications<br>
Parachute Industry Association
