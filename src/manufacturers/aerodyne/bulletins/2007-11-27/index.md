---
pageTitle: Aerodyne Service Bulletin SB271107
date: 2007-11-27
layout: bulletin.html
collection:
  - bulletins
  - aerodyne
short_description: Icon container grommet inspection
description: Icon container grommet inspection
status: Precautionary
identification: "All ICON Harness/Container. Part Number: P120. Manufactured after and including 01 September 2007"
source: https://www.flyaerodyne.com/wp-content/uploads/2017/05/SB271107.pdf
source_accessed: March 10, 2019
---

### Background

A report back from the field revealed that there were some damaged Grommets on an ICON Container. The ICON was returned to the manufacturer where it was determined that the Grommets had been damaged by the Grommet Press.

The Damage can be seen on the inside of the Grommet where the Closing Tool has scratched the Inside Surface of the Grommet. The seating of the Grommet needs to be inspected to ensure correct and flush seating of the Grommet.

This could well be an isolated incident, but as a matter of precaution Aerodyne is recommending all Grommets on the ICONs listed in the Identification paragraph above be Inspected.

### Pictures

 1. Damaged Grommet (see damage on inner ring of grommet)

![Damaged Grommet](damaged_grommet.png)

 2. Undamaged Grommet

 ![Undamaged Grommet](undamaged_grommet.png)

### Authority

Dominic Hayhurst<br>
Technical Director<br>
Aerodyne Research<br>
12649 Race Track Road<br>
Tampa Florida 33626 USA<br>
Tel +1 813 891 6300<br>
dominic@flyaerodyne.com<br>
www.flyaerodyne.com

### Distribution List

 1. Aerodyne Website www.flyaerodyne.com
 2. All Aerodyne Distributors
 3. PIA Technical and Rigging Committee
 4. FPF – French Parachute Federation
