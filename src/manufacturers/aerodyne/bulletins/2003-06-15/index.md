---
pageTitle: Aerodyne Service Bulletin CW03-01
date: 2003-06-15
layout: bulletin.html
collection:
  - bulletins
  - aerodyne
short_description: Capewell Supplied Pins and Ripcords
description: Capewell Supplied Pins and Ripcords
status: MANDATORY TEST – This is a one-time TEST. One of the two tests (either TEST 1 or TEST 2) must be performed and marked per test procedure.
identification: All Capewell Supplied Ripcord Pins and/or Ripcords from November 28, 2001 through to July 15, 2003.
source: https://www.flyaerodyne.com/wp-content/uploads/2017/05/SB_capewell0703A4.pdf
source_accessed: March 9, 2019
---

## Background

Capewell Components LLC has received (3) reports from (3) customers of (4) ripcord pins that broke. All (4) pins were reported to have broken under very low force. All (4) pins are reported to have broken on the blade section of the pin, approximately 1/8” (3 mm) from the shoulder of the pin. A broken pin is pictured below.

Two of these ripcord pins that broke were supplied by Capewell as a completed ripcord assembly. Two of the ripcord pins that broke were supplied as pins to Capewell customers who then assembled the full ripcord. The (4) ripcord pins that broke were delivered to Capewell customers in the first half of 2002.

Capewell has an on-going investigation to determine root cause. No root cause(s) has been determined to date.

Affected Ripcord Pins are: MS70107 (angled), PS70107 (angled), 61C4304 (angled); 55A6480 (terminal)

**WARNING: A DEFECTIVE PIN COULD CAUSE A PARACHUTE SYSTEM TO MALFUNCTION**

![Broken Ripcord Pin](broken_pin.png)

The figure below is provided for clarity to all procedural instructions.

![Pin Anatomy](pin_anatomy_figure.png)

## Test 1: Ripcord pin field test

### Test purpose

The purpose of this test is to make a field determination as to whether the ripcord pin(s) of a packed parachute assembly are defective.

### Test status

**Mandatory Test** - this test must be done prior to the next use of the parachute system

### Authorized repairmen

FAA Senior or Master parachute rigger or foreign equivalent.

### Equipment required

 * Calibrated spring scale with a minimum 20 lbs. (9kg) capacity.
 * Type 2A suspension line 18” (45cm) long.
 * Indelible felt marker

### Comment

This test allows the rigger in the field to make a preliminary determination as to whether the ripcord pin(s) of a packed parachute are defective and prone to break under very low force. It is not necessary to unpack the parachute to perform this test.

### Procedure

 1. Place the parachute on the floor or ground and open the ripcord pin protector flap.
 2. Insure the pin interface element (cone, closing loop, etc), which passes through the grommet that holds the pin in the parachute system, is approximately 3/16” (8 mm) from the pin shoulder.
 3. Take the suspension line and tie the two ends together with an overhand knot forming a loop.
 4. Route the loop around the body of the pin to form a lark’s head knot as close to the cable entrance end of the pin body as possible (Fig. 1).
 5. Secure the other end of the loop to the spring scale.
 6. Apply a load of 15 lbs. (6.8kg) straight up and 90 degrees to the axis of the pin for a period of 3 seconds and then release the load (Fig. 2). Inspect the pin for any deformation or bend. If the pin is straight, continue with step 7 below. If the pin is not straight, as visible without magnification, remove the pin/ripcord from service immediately.
 7. Mark the inspected side of the pin with the felt marker to avoid repeatedly testing the same side of the pin.
 8. Rotate the pin one-quarter turn and test again as in step 6.
 9. Repeat the procedure in step 6 until the pin has been tested in four positions and rotated one-quarter turn in the same direction prior to each test.
 10. Remove the suspension line.
 11. Upon successful completion of test, ripcord handle will be marked with **CW03-01**.
 12. The packing data card must be annotated to show compliance with Capewell Service Bulletin **CW03-01, TEST 1**.
 13. . If the pin should fail the test by breaking, immediately notify Capewell Components (below) for disposition.

#### Figure 1:

 ![Figure 1](figure_1.png)

#### Figure 2:

 ![Figure 2](figure_2.png)

## Test 2: Ripcord pin test for repack

### Test purpose

The purpose of this test is to determine whether the ripcord pin(s) of a repacked parachute assembly are defective.

### Test status

**Mandatory** – this test must be done prior to the next repack of the parachute system

### Authorized repairman

FAA Senior or Master parachute rigger or foreign equivalent.

### Equipment required

 * Calibrated spring scale with a minimum 20 lb. (9kg) capacity (or equivalent mechanism)
 * Aluminum or 300 series stainless steel block
 * Indelible felt marker

### Comment

This test allows the rigger in the field to make a determination as to whether the ripcord pin(s) of a re-packed parachute are defective and prone to break under very low force.

### Procedure

 1. Place pin in test block per Figure 3 (below). NOTE: The pin block hole may be VERTICAL or HORIZONTAL
 2. Apply 11 lb. force 90 degrees perpendicular to the pin blade.
 3. Remove pin from block and inspect:
  * For straightness: Pins in excess of .005 (.12 mm) deformation along the blade of the pin should be removed from service.
  * For surface anomalies using 10x magnification. If any surface anomalies (dents, cracks, chips) exist, the pin should be removed from service.
 4. Mark the inspected side of the pin with the felt marker to avoid repeatedly testing the same side of the pin
 5. Repeat steps 1 through 3, each time rotating the pin 90 degrees in the same direction, until the pin has been tested in 4 directions.
 6. Upon successful completion of test, ripcord handle will be marked with **CW03-01**.
 7. The packing data card must be annotated to show compliance with Capewell Service Bulletin **CW03-01, TEST 2**.
 8. If the pin should fail the test by breaking, immediately notify Capewell Components (below) for disposition.

#### Figure 3

All dimension in inches.

![Diagram](diagram.png)

Customers/users **who can not conduct TEST 1 OR TEST 2**, should remove pins/ripcords from service and return same to Capewell for testing:

Capewell Components<br>
ATTN: Ripcord Pin Test<br>
105 Nutmeg Road South<br>
South Windsor, CT. 06074<br>
USA

**CAPEWELL CONTACT –anyone having questions or needing assistance should contact:
Bill Ehler at 860-610-0700x3360, by fax at 860-610-0120, or by e-mail at bille@capewell.com**

### Authority

Bob Francis, Vice-President/General Manager, Capewell Components
Distribution:

### Distribution

Capewell will distribute this Service Bulletin to:

 * All Capewell Customers
 * Cognizant U. S. Military organizations
 * PIA
 * USPA
 * FAA

This Service Bulletin will be posted on Capewell’s website (www.capewell.com)

Any person or organization that believes further distribution is necessary is urged to do so or to contact
Capewell.
