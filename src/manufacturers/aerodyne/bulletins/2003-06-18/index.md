---
pageTitle: Aerodyne Service Bulletin 2003 07 2
date: 2003-06-18
layout: bulletin.html
collection:
  - bulletins
  - aerodyne
short_description: Aerodyne Ripcord Pins
description: See details.
status: Mandatory
identification: See details.
source: https://www.flyaerodyne.com/wp-content/uploads/2017/05/SB_Aerodyne_ripcord_pin0703.pdf
source_accessed: March 10, 2019
---

### Subject

All Ripcord Pins used on:

 1. Naro Harness and Container Systems (Excluding RSL curved pin)
 2. Teardrop Harness and Container Systems

Must conform to the Capewell Manufacturer Service Bulletin Number CW03-01, Issue Date 15 July 2003.

For clarity please consult your Rigger, Safety Officer or Aerodyne Systems.

### Identification

Part Number:

 * P0138-00
 * P0172-00
 * P0172-10
 * P0172-20
 * P0180-00
 * P0046-00
 * P0046-10
 * P0126-00

Date of manufacture

 * All reserve handles manufactured after 28 November 2001.

### Background

Refer to [Capewell Manufacturer Service Bulletin Number CW03-01 Issue Date 15 July 2003](../2003-06-15/).

### Service Bulletin

Refer to [Capewell Manufacturer Service Bulletin Number CW03-01 Issue Date 15 July 2003](../2003-06-15/).

### Compliance Date

Immediate – 15 July 2003

### Authority

Aerodyne Systems<br>
29 Duiker Road<br>
Canelands<br>
Tel (032) 533 0333<br>
Fax (032) 533 0262<br>
info-indianocean@aerodyne-int.com<br>
www.aerodyne-int.com

### Distribution

Distribution includes but not limited to:

 * National Training and Safety Officer - PASA
 * PASA listserve
 * Drop Zone Safety Officers
