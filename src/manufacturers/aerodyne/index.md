---
pageTitle: Aerodyne Research
date: 2019-03-09
layout: manufacturer.html
collection: manufacturers
description: Aerodyne resources
website: https://www.flyaerodyne.com/
service_bulletins: https://www.flyaerodyne.com/support/
service_bulletins_accessed: March 9, 2019
---

### Manuals

⚠️ Important: NeXgen Icons have serial numbers beginning with `IX`. Pre-neXgen Icons have serial numbers beginning with `IN`.

 * [NeXgen Icon Manual (May 2017 revision)](nexgen_manual.pdf)
 * [Pre-neXgen Icon Manual (Jan 2010 revision)](pre_nexgen_manual.pdf)
