---
layout: index.html
---

# Welcome to skydive.dev

The ultimate parachute rigging database. [Open source](https://gitlab.com/skydive-dev/skydive.dev) and free. Simple and fast. Because you have better things to do.

## Sport Rigs (by part)

<div class="three-columns">

<div class="avoid-column-break">

<h3>Container</h3>

<ul>
  <li>[Aerodyne Research](manufacturers/aerodyne) <ul><li>Icon</li></ul></li>
  <li>[Mirage Systems](manufacturers/mirage) <ul><li>G3, G4</li></ul></li>
  <li>[Rigging Innovations](manufacturers/rigging-innovations) <ul><li>Curv</li></ul></li>
  <li>[Sunpath](manufacturers/sunpath) <ul><li>Javelin</li></ul></li>
  <li>[Sunrise](manufacturers/sunrise) <ul><li>Wings</li></ul></li>
  <li>[UPT](manufacturers/upt) <ul><li>Vector</li></ul></li>
  <li>[Velocity Sports Equipment](manufacturers/velocity) <ul><li>Infinity</li></ul></li>
</ul>

</div>

<div class="avoid-column-break">

<h3>AAD</h3>

<ul>
  <li>[Advanced Aerospace Designs](manufacturers/aad) <ul><li>Vigil</li></ul></li>
  <li>[Airtec](manufacturers/airtec) <ul><li>Cypres</li></ul></li>
  <li>[Aviacom](manufacturers/aviacom) <ul><li>Argus</li></ul></li>
  <li>[MarS a.s.](manufacturers/mars) <ul><li>m2</li></ul></li>
</ul>

</div>

<div class="avoid-column-break">

<h3>Reserve</h3>

<ul>
  <li>[Aerodyne Research](manufacturers/aerodyne) <ul><li>Smart</li></ul></li>
  <li>[Performance Designs](manufacturers/pd) <ul><li>PD-R, Optimum</li></ul></li>
  <li>[Precision Aerodynamics](manufacturers/precision-aerodynamics) <ul><li>Raven</li></ul></li>
</ul>

</div>
</div>

## Tandem Rigs (by part)

### Container

 * Sigma, Micro Sigma (United Parachute Technologies)

## Pilot Rigs (by part)

 * Butler

## Browse more

 * [Browse by Manufacturer](manufacturers)
 * [Browse all Service Bulletins and Airworthiness Directives](sb/all)

## Get notified

Sign up below to be notified when manufacturers publish new service bulletins. We will **never** send you anything other than service bulletins fresh off of the press. We will **never** share your email with anyone else.

<div class="email-signup">
  <form action="https://dev.us20.list-manage.com/subscribe/post?u=54a5acdcda18822773ff01fd6&amp;id=3275f0f0a5" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
    <input type="email" value="" name="EMAIL" class="email-signup__input" placeholder="your-email@example.com">
    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_54a5acdcda18822773ff01fd6_3275f0f0a5" tabindex="-1" value=""></div>
    <input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="email-signup__submit">
  </form>
</div>

<br>
