const Metalsmith  = require('metalsmith');
const collections = require('metalsmith-collections');
const layouts     = require('metalsmith-layouts');
const markdown    = require('metalsmith-markdown');
const permalinks  = require('metalsmith-permalinks');
const aliases = require('metalsmith-aliases');
const sass  = require('metalsmith-sass');
const canonical  = require('metalsmith-canonical');
const extlinks = require('metalsmith-external-links');
const autoprefixer = require('metalsmith-autoprefixer');
const htmlMinifier = require('metalsmith-html-minifier');
const inlineSource = require('metalsmith-inline-source');
const cleanCSS = require('metalsmith-clean-css');
const sitemap = require('metalsmith-sitemap');
const domTransform = require('metalsmith-dom-transform');
const inlineCss = require('metalsmith-inline-css');
const url = require('url');
const moment = require('moment');


const app = Metalsmith(__dirname)
  .metadata({
    title: 'skydive.dev - For riggers, by riggers',
    titleShort: 'skydive.dev',
    description: 'The ultimate parachute rigging database. Open source and free. Simple and fast. Because you have better things to do.'
  })
  .source('./src')
  .destination('./public')
  .clean(false)
  .use(function(files, metalsmith, done) {
    setImmediate(done);
    Object.keys(files).forEach(function(file) {
      files[file].editPath = `https://gitlab.com/skydive-dev/skydive.dev/edit/master/src/${file}`
    });
  })
  .use(markdown({
    gfm: true
  }))
  .use(domTransform({
    transforms: [
      // Set target=_blank on all links
      function(dom, file, { files, metalsmith }, done) {
        Array.from(dom.querySelectorAll('img')).forEach(img => {
          if (img.src) {
            img.setAttribute('data-src', img.src);
            img.removeAttribute('src');
          }
          img.classList.add('lazyload');
        });

        done();
      }
    ]
  }))
  .use(function(files, metalsmith, done) {
    setImmediate(done);
    Object.keys(files).forEach(function(file) {
      if (file.indexOf('.html') !== -1 && file.indexOf('index.html') === -1 && files[file].permalink !== false) {
        var newName = file.replace('.html', '/index.html');
        files[newName] = files[file];

        delete files[file];
      }
    });
  })
  .use(function(files, metalsmith, done) {
    setImmediate(done);

    Object.keys(files).forEach(function(file) {
      if (!files[file].collection || files[file].collection.indexOf('manufacturer') === -1) return;

      files[file].bulletins = [];

      const directory = `${file.replace(/[^\/]*$/, '')}`;
      const bulletinsDirectory = `${directory}bulletins`;

      Object.keys(files)
        .filter(file => file.indexOf(bulletinsDirectory) == 0)
        .filter(file => file.indexOf('.html') !== -1)
        .sort((a, b) => files[a].date < files[b].date)
        .forEach(f => {
          files[f].title = moment(files[f].date).format('LL')
          files[f].url = f.replace(directory, '').replace('index.html', '')

          files[file].bulletins.push(files[f])
        })
    })
  })
  .use(canonical({
    hostname: 'https://skydive.dev',
    omitIndex: true,
    omitTrailingSlashes: false
  }))
  .use(function(files, metalsmith, done) {
    setImmediate(done);
    Object.keys(files).forEach(function(file){
      var data = files[file];
      data.lastmod = data.lastmod || data.date;
    });
  })
  .use(sitemap({
    hostname: 'https://skydive.dev',
    omitExtension: true
  }))
  .use(aliases())
  .use(sass({
    outputDir: 'css',
    outputStyle: 'expanded',
    includePaths: []
  }))
  .use(autoprefixer())
  .use(cleanCSS({
    cleanCSS: {
      rebase: false
    }
  }))
  .use(layouts({
    engine: 'handlebars',
    partials: 'partials'
  }))
  .use(inlineCss())
  .use(inlineSource())
  .use(extlinks({
    domain: 'skydive.dev',
    whitelist: [],
    rel: 'noopener',
    target: '_blank',
    appendRel: true // so we can rel="publisher" social profiles
    // extClass: 'external'
  }))
  .use(htmlMinifier());

if (module.parent) {
  module.exports = app
} else {
  app.build(function (err) { if (err) throw err })
}
